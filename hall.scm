;;; SPDX-FileCopyrightText: 2023 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(hall-description
 (name "x-resize")
 (prefix "")
 (version "0.2")
 (author "Maxim Cournoyer")
 (email "maxim.cournoyer@gmail.com")
 (copyright (2023))
 (synopsis "Dynamic display resizing leveraging udev events")
 (description
  "The @command{x-resize} command detects physical display resolution \nchanges via udev and invokes the @command{xrandr} command to reconfigure \nthe active display resolution accordingly.  It can be used to implement \ndynamic resize support for desktop environments that lack native support \nsuch as Xfce.")
 (home-page "https://gitlab.com/Apteryks/x-resize")
 (license gpl3+)
 (dependencies ("coreutils"
                ("guile-lib" (logging logger))
                ("guile-udev" (udev udev))
                "xrandr"))
 (skip ())
 (features
  ((guix #t)
   (use-guix-specs-for-dependencies #t)))
 (files (libraries
         ((directory
           "x-resize"
           ((in-file "config.scm")
            (scheme-file "config")
            (scheme-file "hconfig")
            (scheme-file "utils")
            (scheme-file "logging")
            (scheme-file "main")))))
        (tests ())
        (programs
         ((directory "scripts" ((in-file "x-resize")))))
        (documentation
         ((directory "LICENSES"
                     ((text-file "GPL-3.0-or-later.txt")
                      (text-file "CC0-1.0.txt")))
          ;; XXX: The XDG autostart file is listed as 'documentation',
          ;; as there doesn't appear to be a way currently in Hall to
          ;; list installable architecture-independent files (see:
          ;; https://gitlab.com/a-sassmannshausen/guile-hall/-/issues/83).
          (text-file "x-resize.desktop.template")
          (text-file "COPYING")
          (text-file "HACKING")
          (org-file "README")
          (symlink "README" "README.org")
          (text-file "NEWS")
          (text-file "AUTHORS")
          (text-file "ChangeLog")))
        (infrastructure
         ((scheme-file "hall")
          (text-file ".gitignore")
          (scheme-file "guix")
          (directory
           "build-aux"
           ((tex-file "texinfo")
            (text-file "missing")
            (text-file "install-sh")
            (scheme-file "test-driver")))
          (autoconf-file "configure")
          (automake-file "Makefile")
          (in-file "pre-inst-env")))))
