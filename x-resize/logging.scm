;;; SPDX-FileCopyrightText: 2023 - 2024 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (x-resize logging)
  #:use-module (logging logger)
  #:use-module (logging rotating-log)
  #:use-module (logging port-log)

  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (srfi srfi-34)

  #:re-export (log-msg)                 ;for convenience

  #:export (setup-logging
            shutdown-logging))

;;; Copied from (guix build utils).
(define (mkdir-p dir)
  "Create directory DIR and all its ancestors."
  (define absolute?
    (string-prefix? "/" dir))

  (define not-slash
    (char-set-complement (char-set #\/)))

  (let loop ((components (string-tokenize dir not-slash))
             (root       (if absolute?
                             ""
                             ".")))
    (match components
      ((head tail ...)
       (let ((path (string-append root "/" head)))
         (catch 'system-error
           (lambda ()
             (mkdir path)
             (loop tail path))
           (lambda args
             (if (= EEXIST (system-error-errno args))
                 (loop tail path)
                 (apply throw args))))))
      (() #t))))

(define* (setup-logging #:key debug? log-file)
  "Configure and open logger."

  (when log-file
    ;; XXX: Perhaps this should be handled by guile-lib's logging
    ;; library.
    (false-if-exception (mkdir-p (dirname log-file))))

  (let ((lgr       (make <logger>))
        (rotating  (and log-file
                        (make <rotating-log> #:file-name log-file)))
        (console   (make <port-log> #:port (current-error-port))))

    (register-logger! "main" lgr)

    ;; Register logging handlers.
    (when rotating
      (add-handler! lgr rotating))
    (add-handler! lgr console)

    ;; Configure the verbosity.
    (unless debug?
      (disable-log-level! lgr 'DEBUG))

    ;; ... and activate the configured logger.
    (set-default-logger! lgr)
    (mkdir-p (dirname log-file))
    (guard (c ((eq? (exception-kind c) 'system-error)
               (log-msg 'WARNING "could not write " log-file
                        "; not logging to file")))
      (open-log! lgr))))

(define (shutdown-logging)
  "Flush and destroy logger."
  (flush-log)
  (close-log!)
  (set-default-logger! #f))
