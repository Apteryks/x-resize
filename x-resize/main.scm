;;; SPDX-FileCopyrightText: 2023 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (x-resize main)
  #:use-module (x-resize config)
  #:use-module (x-resize logging)
  #:use-module (x-resize utils)

  #:use-module (udev udev)
  #:use-module (udev monitor)

  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)

  #:export (main))

(define %option-spec
  '((debug (single-char #\d) (value #f))
    (help  (single-char #\h) (value #f))
    (log-file (single-char #\l) (value #t))))

(define (drm-event-callback device)
  ;; TODO: Handle any exception, log backtrace.
  (let ((old-resolutions (get-screen-resolutions)))
    (invoke %xrandr-command "-s" "0")
    (let* ((new-resolutions (get-screen-resolutions))
           (changed (fold-right (lambda (old new result)
                                  (if (equal? old new)
                                      result
                                      (cons new result)))
                                '()
                                old-resolutions
                                new-resolutions)))
      (log-msg 'DEBUG "newly changed resolutions: " changed)
      (if (null? changed)
          (log-msg 'WARNING "no resolution update despite drm event")
          (for-each (match-lambda
                      ((index . (width . height))
                       (match (assoc-ref old-resolutions index)
                         ((old-width . old-height)
                          (log-msg 'INFO "screen " index " resized from "
                                   old-width "x" old-height " to "
                                   width "x" height)))))
                    changed)))))

(define (main args)
  "The main routine."
  (define options (getopt-long args %option-spec))
  (define log-file (option-ref options 'log-file %log-file))

  (when (option-ref options 'help #f)
    (display "\
x-resize [options]
       -d, --debug      Output debug messages
       -h, --help       Display this help
       -l, --log-file   The file name of the log file
")
    (exit 0))

  (setup-logging #:debug? (option-ref options 'debug #f)
                 #:log-file (option-ref options 'log-file %log-file))

  (log-msg 'INFO "x-resize started; logging to " log-file)

  (let* ((udev (make-udev))
         (monitor (make-udev-monitor udev
                                     #:filter (list "drm" "drm_minor")
                                     #:callback drm-event-callback)))
    (udev-monitor-start-scanning! monitor)
    (while #t (sleep 1)))

  (log-msg 'INFO "x-resize stopped")

  (shutdown-logging))
