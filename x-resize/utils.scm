;;; SPDX-FileCopyrightText: 2023 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (x-resize utils)
  #:use-module (x-resize config)
  #:use-module (x-resize logging)

  #:use-module (ice-9 popen)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)

  #:export (die
            invoke
            get-screen-resolutions))

(define (die . message)
  "Exit with an error MESSAGE."
  (apply log-msg 'ERROR message)
  (exit 1))

(define (invoke command . args)
  "Invoke COMMAND with its arguments ARGS and return its output.
Exit with an error if it fails."
  (let* ((pipe1 (pipe))
         (pipe2 (pipe))
         (pid (spawn command (cons command args)
                     #:output (cdr pipe1)
                     #:error (cdr pipe2)
                     ;; Ensure the locale used is the default (US),
                     ;; for stable output.
                     #:environment (cons "LANG=C" (environ)))))
    ;; Close the output side of the pipes in the parent.
    (close-port (cdr pipe1))
    (close-port (cdr pipe2))

    (let* ((status   (cdr (waitpid pid)))
           (output   (get-string-all (car pipe1)))
           (error    (get-string-all (car pipe2)))
           (exit-val (status:exit-val status)))
      (close-port (car pipe1))
      (close-port (car pipe2))

      (log-msg 'DEBUG command " " args " output: \n" output)

      (unless (zero? exit-val)
        (die (format #f "command `~a' with args `~a' exited with `~a' \
and stderr~%:~a" command args exit-val error)))

      output)))

(define (get-screen-resolutions)
  "Return the current Xrandr screen resolutions, as a list of (heigh
. width) pairs.  If there are multiple screens, each screen index
correspond to the index in the returned list."
  (let* ((output (invoke %xrandr-command "--current"))
         (lines (string-split (string-trim-right output) #\newline))
         (rx (make-regexp "^Screen ([[:digit:]]+):.*\
current ([[:digit:]]+) x ([[:digit:]]+)")))
    (filter-map (lambda (line)
                  (and-let* ((m (regexp-exec rx line)))
                    (cons (match:substring m 1) ;index
                          (cons (match:substring m 2) ;width
                                (match:substring m 3))))) ;height
                lines)))
