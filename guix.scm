;;; SPDX-FileCopyrightText: 2023 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules
  (gnu packages)
  (gnu packages autotools)
  (gnu packages base)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo)
  (gnu packages xorg)
  (guix build-system gnu)
  (guix download)
  (guix gexp)
  ((guix licenses) #:prefix license:)
  (guix packages)
  (srfi srfi-1))

(package
  (name "x-resize")
  (version "0.2")
  (source
    (local-file
      (dirname (current-filename))
      #:recursive?
      #t
      #:select?
      (lambda (file stat)
        (not (any (lambda (my-string)
                    (string-contains file my-string))
                  (list ".git" ".dir-locals.el" "guix.scm"))))))
  (build-system gnu-build-system)
  (arguments
    (list #:modules
          `(((guix build guile-build-system)
             #:select
             (target-guile-effective-version))
            ,@%gnu-build-system-modules)
          #:phases
          (with-imported-modules
            `((guix build guile-build-system)
              ,@%gnu-build-system-modules)
            (gexp (modify-phases
                    %standard-phases
                    (add-after
                      'install
                      'hall-wrap-binaries
                      (lambda* (#:key inputs #:allow-other-keys)
                        (let* ((version (target-guile-effective-version))
                               (site-ccache
                                 (string-append
                                   "/lib/guile/"
                                   version
                                   "/site-ccache"))
                               (site (string-append
                                       "/share/guile/site/"
                                       version))
                               (dep-path
                                 (lambda (env path)
                                   (list env
                                         ":"
                                         'prefix
                                         (cons (string-append
                                                 (ungexp output)
                                                 path)
                                               (map (lambda (input)
                                                      (string-append
                                                        (assoc-ref
                                                          inputs
                                                          input)
                                                        path))
                                                    (list "guile-lib"
                                                          "guile-udev"))))))
                               (bin (string-append (ungexp output) "/bin/")))
                          (for-each
                            (lambda (file)
                              (wrap-program
                                (string-append bin file)
                                (dep-path "GUILE_LOAD_PATH" site)
                                (dep-path
                                  "GUILE_LOAD_COMPILED_PATH"
                                  site-ccache)
                                (dep-path "GUILE_EXTENSIONS_PATH" "/lib")))
                            (list "x-resize"))))))))))
  (native-inputs
    (list autoconf automake pkg-config texinfo))
  (inputs (list guile-3.0))
  (propagated-inputs
    (list coreutils guile-lib guile-udev xrandr))
  (synopsis
    "Dynamic display resizing leveraging udev events")
  (description
    "The @command{x-resize} command detects physical display resolution \nchanges via udev and invokes the @command{xrandr} command to reconfigure \nthe active display resolution accordingly.  It can be used to implement \ndynamic resize support for desktop environments that lack native support \nsuch as Xfce.")
  (home-page
    "https://gitlab.com/Apteryks/x-resize")
  (license license:gpl3+))

